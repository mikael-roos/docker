server {
    root /public; # That location corresponds to one mapped in docker-compose.yml
    index index.html index.htm;
    server_name localhost;

    location ~ {
        autoindex on;
    }

    location ~ /\.ht {
        deny all;
    }

    location ~*\.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|woff|css|js|xml|txt)$ {
        expires 7d;
        access_log off;
        add_header Cache-Control "public";
    }

    location ~ \.(?:swf|pdf|mov|fla|zip|rar|doc|docx|docm)$ {
        try_files $uri =404;
    }

    location /hello-world/ {
        proxy_pass http://localhost:5001/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade            $http_upgrade;
        proxy_set_header Connection         'upgrade';

        # Added after the recording
        proxy_set_header Host                 $host;
        proxy_set_header X-Real-IP            $remote_addr;
        proxy_set_header X-Forwarded-For      $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto    $scheme;
        proxy_set_header X-Forwarded-Host     $host;
        proxy_set_header X-Forwarded-Port     $server_port;
    }
}
